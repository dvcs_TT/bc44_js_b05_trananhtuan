function calcTax() {
    var e = document.getElementById("inputName2").value,
        n = document.getElementById("inputSalary").value - 4e6 - 16e5 * document.getElementById("inputUser").value,
        t = 0;
    n > 0 && n <= 6e7
        ? (t = 0.05 * n)
        : n > 6e7 && n <= 12e7
        ? (t = 0.1 * n)
        : n > 12e7 && n <= 21e7
        ? (t = 0.15 * n)
        : n > 21e7 && n <= 384e6
        ? (t = 0.2 * n)
        : n > 384e6 && n <= 624e6
        ? (t = 0.25 * n)
        : n > 624e6 && n <= 96e7
        ? (t = 0.3 * n)
        : n > 96e7
        ? (t = 0.35 * n)
        : alert("Số tiền thu nhập không hợp lệ"),
        (t = new Intl.NumberFormat("vn-VN").format(t)),
        (document.getElementById("txtTax").innerHTML = "Họ tên: " + e + "; Tiền thuế thu nhập cá nhân: " + t + " VND");
}